# Manual solver for the IPC domain Termes

Florian Pommerening <florian.pommerening@unibas.ch>

We used this manual solver to come up with satisficing solutions for instances
where we could not find good solutions automatically.

An automated solver is available as part of the generator for this domain:
https://bitbucket.org/ipc2018-classical/generate-termes


## Controls
Set up the board size by entering width and height and clicking reset.

Use the arrow keys to move around. Movement is only possible if the height of
the current and target position differs by at most one.

To create a box or destroy the one the robot is carrying, press space while the
robot is at the depot. To place a box or pick up box, hold Ctrl while pressing
an arrow key. Boxes can only be placed if the target position is the same
height as the current position and they can only be picked up if they rest on
the same level as the robot (including the box, the position to pick up must be
one higher).

There are two text areas above and below the reset button. The one above is the
"past" and keeps track of all steps performed so far. If you reach your goal
position, this box contains a plan to reach this position.

Clicking the "<" button undoes the last step and moves this step into the lower
text area, the "future". Clicking ">" will try to redo the first step in the
future. If it is not applicable, nothing happens. It is possible to simulate
plans by copying them into the future and repeatedly pressing ">".

