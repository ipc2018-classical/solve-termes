#!/usr/bin/env python

import tkinter as tk
import re

CELL_SIZE = 50

class GameGrid(object):
    def __init__(self, root, columns, rows):
        root.title("Termes")
        frame_text = tk.Frame(root)
        tk.Grid.columnconfigure(frame_text, 0, weight=1)
        tk.Grid.columnconfigure(frame_text, 1, weight=1)
        tk.Grid.rowconfigure(frame_text, 0, weight=1)

        frame_past = tk.Frame(frame_text)
        scrollbar_past = tk.Scrollbar(frame_past)
        self.textarea_past = tk.Text(frame_past, height=10, width=50)
        scrollbar_past.pack(side=tk.RIGHT, fill=tk.Y)
        self.textarea_past.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        scrollbar_past.config(command=self.textarea_past.yview)
        self.textarea_past.config(yscrollcommand=scrollbar_past.set, state=tk.DISABLED)
        frame_past.pack(side=tk.TOP, fill=tk.BOTH, expand=True)

        frame_buttons = tk.Frame(frame_text)
        button_forward = tk.Button(frame_buttons, text=">", command=self.do_step_from_list)
        button_forward.pack(side=tk.RIGHT, padx=5, pady=5)
        button_backward = tk.Button(frame_buttons, text="<", command=self.undo_step_from_list)
        button_backward.pack(side=tk.RIGHT, padx=5, pady=5)

        self.entry_width = tk.Entry(frame_buttons, width=3)
        self.entry_width.insert(0, columns)
        self.entry_width.pack(side=tk.LEFT, padx=5, pady=5)
        self.entry_height = tk.Entry(frame_buttons, width=3)
        self.entry_height.insert(0, rows)
        self.entry_height.pack(side=tk.LEFT, padx=5, pady=5)
        button_reset = tk.Button(frame_buttons, text="Reset", command=self.reset)
        button_reset.pack(side=tk.LEFT, padx=5, pady=5)
        self.label_plan_length = tk.Label(frame_buttons)
        self.label_plan_length.pack(side=tk.LEFT, padx=5, pady=5)
        frame_buttons.pack(side=tk.TOP, fill=tk.X)

        frame_future = tk.Frame(frame_text)
        scrollbar_future = tk.Scrollbar(frame_future)
        self.textarea_future = tk.Text(frame_future, height=10, width=50)
        scrollbar_future.pack(side=tk.RIGHT, fill=tk.Y)
        self.textarea_future.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        scrollbar_future.config(command=self.textarea_future.yview)
        self.textarea_future.config(yscrollcommand=scrollbar_future.set)
        frame_future.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)
        frame_text.pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)

        self.canvas = tk.Canvas(root)
        self.canvas.pack(side=tk.LEFT)
        self.canvas.after(0, self.draw)

        root.bind("<Up>", lambda e: self.move(0, -1))
        root.bind("<Down>", lambda e: self.move(0, 1))
        root.bind("<Left>", lambda e: self.move(-1, 0))
        root.bind("<Right>", lambda e: self.move(1, 0))
        root.bind("<Control-Up>", lambda e: self.drop_down(0, -1) if self.carries_block else self.pick_up(0, -1))
        root.bind("<Control-Down>", lambda e: self.drop_down(0, 1) if self.carries_block else self.pick_up(0, 1))
        root.bind("<Control-Left>", lambda e: self.drop_down(-1, 0) if self.carries_block else self.pick_up(-1, 0))
        root.bind("<Control-Right>", lambda e: self.drop_down(1, 0) if self.carries_block else self.pick_up(1, 0))
        root.bind("<space>", lambda e: self.destroy_block() if self.carries_block else self.create_block())

        self.reset()

    def add_action(self, action):
        self.textarea_past.config(state=tk.NORMAL)
        self.textarea_past.insert(tk.END, action + "\n")
        self.textarea_past.config(state=tk.DISABLED)
        self.textarea_past.see("end")

    def get_height(self, x, y):
        return self.grid[y][x]

    def get_current(self):
        cx, cy = self.robot_pos
        return cx, cy, self.get_height(cx, cy)

    def get_neighbor(self, dx, dy):
        cx, cy = self.robot_pos
        nx, ny = cx + dx, cy + dy
        if 0 <= nx < self.columns and 0 <= ny < self.rows:
            return nx, ny, self.get_height(nx, ny)
        return None, None, None

    def move(self, dx, dy, silent=False):
        cx, cy, ch = self.get_current()
        nx, ny, nh = self.get_neighbor(dx, dy)
        if nh is not None and abs(ch - nh) <= 1:
            self.robot_pos = nx, ny
            if ch < nh:
                action = "(move-up pos-%d-%d n%d pos-%d-%d n%d)" % (cx, cy, ch, nx, ny, nh)
            elif ch > nh:
                action = "(move-down pos-%d-%d n%d pos-%d-%d n%d)" % (cx, cy, ch, nx, ny, nh)
            else:
                action = "(move pos-%d-%d pos-%d-%d n%d)" % (cx, cy, nx, ny, nh)
            if not silent:
                self.add_action(action)
            return True
        return False

    def pick_up(self, dx, dy, silent=False):
        cx, cy, ch = self.get_current()
        nx, ny, nh = self.get_neighbor(dx, dy)
        if (not self.carries_block and nh is not None and ch + 1 == nh):
            self.carries_block = True
            self.grid[ny][nx] -= 1
            if not silent:
                self.add_action("(remove-block pos-%d-%d pos-%d-%d n%d n%d)" % (cx, cy, nx, ny, nh, nh-1))
            return True
        return False

    def drop_down(self, dx, dy, silent=False):
        cx, cy, ch = self.get_current()
        nx, ny, nh = self.get_neighbor(dx, dy)
        if (self.carries_block and nh is not None and (nx, ny) != self.depot_pos and ch == nh):
            self.carries_block = False
            self.grid[ny][nx] += 1
            if not silent:
                self.add_action("(place-block pos-%d-%d pos-%d-%d n%d n%d)" % (cx, cy, nx, ny, nh, nh+1))
            return True
        return False

    def create_block(self, silent=False):
        if (not self.carries_block and self.depot_pos == self.robot_pos):
            self.carries_block = True
            if not silent:
                self.add_action("(create-block pos-%d-%d)" % self.robot_pos)
            return True
        return False

    def destroy_block(self, silent=False):
        if (self.carries_block and self.depot_pos == self.robot_pos):
            self.carries_block = False
            if not silent:
                self.add_action("(destroy-block pos-%d-%d)" % self.robot_pos)
            return True
        return False

    def try_apply_action(self, action, parameters, silent=False):
        if action.startswith("move"):
            if action == "move":
                cx, cy, nx, ny, nh = parameters
                ch = nh
            else:
                cx, cy, ch, nx, ny, nh = parameters
            if ((cx, cy, ch) == self.get_current() and
                abs(cx - nx) + abs(cy - ny) == 1 and
                nh == self.get_height(nx, ny) and
                ((ch == nh and action == "move") or
                 (ch + 1 == nh and action == "move-up") or
                 (ch - 1 == nh and action == "move-down")
                 )):
                     return self.move(nx - cx, ny - cy, silent=silent)
        elif action == "remove-block":
            cx, cy, nx, ny, nh, nh_after = parameters
            if ((cx, cy, nh_after) == self.get_current() and
                abs(cx - nx) + abs(cy - ny) == 1 and
                nh == self.get_height(nx, ny) and
                nh - 1 == nh_after):
                     return self.pick_up(nx - cx, ny - cy, silent=silent)
        elif action == "place-block":
            cx, cy, nx, ny, nh, nh_after = parameters
            if ((cx, cy, nh) == self.get_current() and
                abs(cx - nx) + abs(cy - ny) == 1 and
                nh == self.get_height(nx, ny) and
                nh + 1 == nh_after):
                     return self.drop_down(nx - cx, ny - cy, silent=silent)
        elif action == "create-block":
            cx, cy = parameters
            if (cx, cy) == self.robot_pos:
                     return self.create_block(silent=silent)
        elif action == "destroy-block":
            cx, cy = parameters
            if (cx, cy) == self.robot_pos:
                     return self.destroy_block(silent=silent)

    def undo_action(self, action, parameters):
        if action == "move":
            cx, cy, nx, ny, nh = parameters
            self.try_apply_action("move", [nx, ny, cx, cy, nh], silent=True)
        elif action == "move-up":
            cx, cy, ch, nx, ny, nh = parameters
            self.try_apply_action("move-down", [nx, ny, nh, cx, cy, ch], silent=True)
        elif action == "move-down":
            cx, cy, ch, nx, ny, nh = parameters
            self.try_apply_action("move-up", [nx, ny, nh, cx, cy, ch], silent=True)
        elif action == "remove-block":
            cx, cy, nx, ny, nh, nh_after = parameters
            self.try_apply_action("place-block", [cx, cy, nx, ny, nh_after, nh], silent=True)
        elif action == "place-block":
            cx, cy, nx, ny, nh, nh_after = parameters
            self.try_apply_action("remove-block", [cx, cy, nx, ny, nh_after, nh], silent=True)
        elif action == "create-block":
            cx, cy = parameters
            self.try_apply_action("destroy-block", [cx, cy], silent=True)
        elif action == "destroy-block":
            cx, cy = parameters
            self.try_apply_action("create-block", [cx, cy], silent=True)

    def parse_action(self, action):
        m = re.match("\((\S*) .*\)", action)
        if m:
            return m.group(1), [int(x) for x in re.findall("\d+", action)]
        return None, None

    def do_step_from_list(self):
        action = self.textarea_future.get("1.0", "2.0-1c")
        name, parameters = self.parse_action(action)
        if name and self.try_apply_action(name, parameters):
            self.textarea_future.delete("1.0", "2.0")

    def undo_step_from_list(self):
        action = self.textarea_past.get("end-2c linestart", "end-2c lineend")
        name, parameters = self.parse_action(action)
        if name:
            self.undo_action(name, parameters)
            self.textarea_past.config(state=tk.NORMAL)
            self.textarea_past.delete("end-2c linestart", "end-2c lineend+1c")
            self.textarea_past.config(state=tk.DISABLED)
            self.textarea_future.insert("1.0", action + "\n")

    def get_plan_length(self):
        return int(self.textarea_past.index('end-1c').split('.')[0]) - 1

    def draw(self):
        for x  in range(self.columns):
            for y  in range(self.rows):
                label_text = str(self.get_height(x, y))
                if (x, y) == self.depot_pos:
                    label_text = "Dep."
                self.canvas.itemconfig(self.cell_labels[y][x], text=label_text)
                color = "white"
                if (x, y) == self.robot_pos:
                    if self.carries_block:
                        color = "red"
                    else:
                        color = "orange"
                self.canvas.itemconfig(self.cells[y][x], fill=color)

        self.canvas.update()
        self.canvas.after(20, self.draw)

        self.label_plan_length.config(text="%d Steps" % self.get_plan_length())

    def reset(self):
        w = int(self.entry_width.get())
        h = int(self.entry_height.get())
        self.columns = w
        self.rows = h
#        self.depot_pos = (int((w-0.5)/2), 0)
        self.depot_pos = (int(w/2), 0)
        self.robot_pos = self.depot_pos
        self.carries_block = False
        self.grid = [[0 for _  in range(self.columns)] for _  in range(self.rows)]
        self.textarea_past.config(state=tk.NORMAL)
        self.textarea_past.delete("1.0", "end")
        self.textarea_past.config(state=tk.DISABLED)
        self.textarea_future.delete("1.0", "end")

        self.canvas.config(width=self.columns*CELL_SIZE, height=self.rows*CELL_SIZE)
        self.cells = [[self.canvas.create_rectangle(
                            x*CELL_SIZE, y*CELL_SIZE, (x+1)*CELL_SIZE, (y+1)*CELL_SIZE,
                            outline="white")
                        for x  in range(self.columns)] for y  in range(self.rows)]

        self.cell_labels = [[self.canvas.create_text(
                            ((x+0.5)*CELL_SIZE, (y + 0.5)*CELL_SIZE),
                            text=str(self.grid[y][x]))
                        for x  in range(self.columns)] for y  in range(self.rows)]


root = tk.Tk()
state = GameGrid(root, 4, 4)
root.mainloop()
